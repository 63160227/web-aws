import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import * as bcrypt from 'bcrypt';
const saltOrRounds = 10;

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(createUserDto.password, salt);
    createUserDto.password = hash;
    return this.usersRepository.save(createUserDto);
  }

  findAll() {
    return this.usersRepository.find({ relations: ['employee'] });
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
      relations: ['employee'],
    });
  }

  findOneByUserName(login: string) {
    return this.usersRepository.findOne({ where: { email: login } });
  }

  async loginUsername(login: string, password: string) {
    const user = await this.usersRepository.findOne({
      where: { email: login },
    });
    if (user) {
      const isMatch = await bcrypt.compare(password, user.password);
      if (user && isMatch) {
        return this.usersRepository.findOne({
          where: { email: login },
          relations: ['employee'],
        });
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    try {
      if (updateUserDto.password !== undefined) {
        const salt = await bcrypt.genSalt();
        const hash = await bcrypt.hash(updateUserDto.password, salt);
        updateUserDto.password = hash;
      }
      const updatedUser = await this.usersRepository.save({
        id,
        ...updateUserDto,
      });
      return updatedUser;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const user = await this.usersRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedUser = await this.usersRepository.remove(user);
      return deletedUser;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
