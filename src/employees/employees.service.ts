import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';

@Injectable()
export class EmployeesService {
  constructor(
    @InjectRepository(Employee)
    private EmployeelsRepository: Repository<Employee>,
  ) {}

  async create(createEmployeeDto: CreateEmployeeDto) {
    const employee = await this.EmployeelsRepository.save(createEmployeeDto);
    return employee;
  }

  async findAll() {
    const employee = await this.EmployeelsRepository.find();
    return employee;
  }

  async findOne(id: number) {
    const employee = await this.EmployeelsRepository.findOne({
      where: { id: id },
    });
    if (!employee) {
      throw new NotFoundException();
    }
    return employee;
  }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    const employee = await this.EmployeelsRepository.findOneBy({ id: id });
    if (!employee) {
      throw new NotFoundException();
    }
    const updateEmployee = { ...employee, ...updateEmployeeDto };
    return this.EmployeelsRepository.save(updateEmployee);
  }

  async remove(id: number) {
    const employee = await this.EmployeelsRepository.findOneBy({ id: id });
    if (!employee) {
      throw new NotFoundException();
    }
    return this.EmployeelsRepository.softRemove(employee);
  }
}
