import { OrderItem } from 'src/orders/entities/order-item';
import { Order } from 'src/orders/entities/order.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Table {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  amount: number;
  @Column()
  status: string;
  @OneToMany(() => Order, (order) => order.table)
  order: Order[];

  @OneToMany(() => OrderItem, (orderItem) => orderItem.table)
  orderItems: OrderItem[];
}
