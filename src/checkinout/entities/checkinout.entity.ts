// import { Employee } from 'src/employees/entities/employee.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Checkinout {
  @PrimaryGeneratedColumn()
  id: number;
  @CreateDateColumn()
  timestart: Date;
  @Column()
  timeend: Date = new Date();
  @Column()
  hour: number;
  @Column()
  status: string;
  @ManyToOne(() => Employee, (employee) => employee.checkinout)
  employee: Employee;
  @UpdateDateColumn()
  updatedAt: Date;
  @DeleteDateColumn()
  deleteddAt: Date;
}
