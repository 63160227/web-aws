import { IsNotEmpty, Length } from 'class-validator';
import { Employee } from 'src/employees/entities/employee.entity';

export class CreateSalaryDto {
  salary: number;
  date: Date;
  employee: Employee;
}
