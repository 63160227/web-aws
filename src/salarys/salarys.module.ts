import { Module } from '@nestjs/common';
import { SalarysService } from './salarys.service';
import { SalarysController } from './salarys.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Salary } from './entities/salary.entity';
import { Checkinout } from 'src/checkinout/entities/checkinout.entity';
import { Employee } from 'src/employees/entities/employee.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Salary, Checkinout, Employee])],
  controllers: [SalarysController],
  providers: [SalarysService],
})
export class SalarysModule {}
