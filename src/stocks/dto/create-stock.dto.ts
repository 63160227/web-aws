import { IsInt, IsNotEmpty, Length, Min } from 'class-validator';

export class CreateStockDto {
  @Length(4, 10)
  @IsNotEmpty()
  matCode: string;

  @Length(3, 25)
  @IsNotEmpty()
  name: string;

  @IsInt()
  @Min(1)
  @IsNotEmpty()
  balance: number;

  @Length(3, 25)
  @IsNotEmpty()
  unit: string;

  @Length(3, 25)
  @IsNotEmpty()
  category: string;
}
