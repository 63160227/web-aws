import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Stock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '10',
  })
  matCode: string;

  @Column({
    length: '25',
  })
  name: string;

  @Column({
    type: 'int',
  })
  balance: number;

  @Column({
    length: '25',
  })
  unit: string;

  @Column({
    length: '25',
  })
  category: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
