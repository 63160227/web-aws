import { IsNotEmpty, Length, IsNumber, Min } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;
  img = 'no_image.jpg';
  // @IsNumber()
  // @Min(0)
  price: number;
}
