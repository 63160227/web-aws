import { Injectable } from '@nestjs/common';
import { CreateProductQueueDto } from './dto/create-product-queue.dto';
import { UpdateProductQueueDto } from './dto/update-product-queue.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductQueue } from './entities/product-queue.entity';
import { Repository } from 'typeorm';
import { OrderItem } from 'src/orders/entities/order-item';

@Injectable()
export class ProductQueueService {
  constructor(
    @InjectRepository(ProductQueue)
    private productQueueRepository: Repository<ProductQueue>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
  ) {}
  async create(createProductQueueDto: CreateProductQueueDto) {
    const orderItem = await this.orderItemsRepository.findOneBy({
      id: createProductQueueDto.orderItem.id,
    });
    const productQueue: ProductQueue = new ProductQueue();
    productQueue.name = createProductQueueDto.name;
    productQueue.note = createProductQueueDto.note;
    productQueue.price = createProductQueueDto.price;
    productQueue.status = createProductQueueDto.status;
    productQueue.orderItem = orderItem;
    return this.productQueueRepository.save(createProductQueueDto);
  }

  findAll() {
    return this.productQueueRepository
      .createQueryBuilder('productQueue')
      .leftJoinAndSelect('productQueue.orderItem', 'orderItem')
      .leftJoinAndSelect('orderItem.product', 'product')
      .where('productQueue.status NOT IN (:...status)', {
        status: ['ชำระเงินแล้ว'],
      })
      .getMany();
  }
  findAllStatus(status: string) {
    return this.productQueueRepository
      .createQueryBuilder('productQueue')
      .leftJoinAndSelect('productQueue.orderItem', 'orderItem')
      .leftJoinAndSelect('orderItem.order', 'order')
      .leftJoinAndSelect('orderItem.product', 'product')
      .leftJoinAndSelect('order.table', 'table')
      .where('productQueue.status = :status', { status: status })
      .getMany();
  }

  async changeStatus(id: number[], status: string) {
    id.forEach(async (value: number) => {
      const productQueue = await this.productQueueRepository.find({
        where: { id: value },
      });

      await this.productQueueRepository.update(id, { status: status });
    });
    return 'Success';
  }

  findOne(id: number) {
    return `This action returns a #${id} productQueue`;
  }

  update(id: number, updateProductQueueDto: UpdateProductQueueDto) {
    return `This action updates a #${id} productQueue`;
  }

  remove(id: number) {
    return `This action removes a #${id} productQueue`;
  }
}
