import { Test, TestingModule } from '@nestjs/testing';
import { ProductQueueService } from './product-queue.service';

describe('ProductQueueService', () => {
  let service: ProductQueueService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProductQueueService],
    }).compile();

    service = module.get<ProductQueueService>(ProductQueueService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
