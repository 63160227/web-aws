import { Test, TestingModule } from '@nestjs/testing';
import { ProductQueueController } from './product-queue.controller';
import { ProductQueueService } from './product-queue.service';

describe('ProductQueueController', () => {
  let controller: ProductQueueController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductQueueController],
      providers: [ProductQueueService],
    }).compile();

    controller = module.get<ProductQueueController>(ProductQueueController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
